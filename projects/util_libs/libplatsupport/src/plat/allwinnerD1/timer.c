/*
 * Copyright 2019, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#include <stdio.h>
#include <assert.h>
#include <errno.h>
#include <utils/util.h>
#include <platsupport/timer.h>
#include <platsupport/fdt.h>
#include <platsupport/plat/timer.h>
#include <utils/frequency.h>

#include "../../ltimer.h"

#define FDIV_1 1
#define FDIV_2 2
#define FDIV_4 4
#define FDIV_8 8
#define FDIV_16 16
#define FDIV_32 32
#define FDIV_64 64
#define FDIV_128 128


const uint64_t freq_divd[] = 
{
    24000000ull/FDIV_1, 
    24000000ull/FDIV_2, 
    24000000ull/FDIV_4, 
    24000000ull/FDIV_8, 
    24000000ull/FDIV_16, 
    24000000ull/FDIV_32,
    24000000ull/FDIV_64, 
    24000000ull/FDIV_128, 
    0ull
};

//debug method
static void print_regs(sun4i_timer_t * sun4i)
{
    printf("TMR_cb_event:        >> 0x%08x\n", sun4i->user_cb_event);
    printf("TMR_clk_div:         >> 0x%08x\n", sun4i->clk_div);
    printf("TMR_INTV_VALUE_REG   >> 0x%08x\n", sun4i->hw.tm_reg->tmr_intv_value_reg);
    printf("TMR_CUR_VALUE_REG    >> 0x%08x\n", sun4i->hw.tm_reg->tmr_cur_value_reg);
    printf("TMR_CTRL_REG         >> 0x%08x\n", sun4i->hw.tm_reg->tmr_ctrl_reg);
    printf("TMR_IRQ_EN_REG       >> 0x%08x\n", sun4i->hw.tm_irq->irq_enbl_reg);
    printf("TMR_IRQ_STA_REG      >> 0x%08x\n", sun4i->hw.tm_irq->irq_sta_reg);
}

int sun4i_stop(sun4i_timer_t * sun4i)
{
    if (sun4i == NULL) {
        return EINVAL;
    }

    // disable timer 
    sun4i->hw.tm_reg->tmr_ctrl_reg &= ~SUN4I_TMR_EN_BIT & ~SUN4I_TMR_CLK_SRC_MASK;

    return 0;
}

uint64_t sun4i_get_time(sun4i_timer_t * sun4i)
{
    if (sun4i == NULL) {
        return EINVAL;
    }
    
    uint32_t time = sun4i->hw.tm_reg->tmr_cur_value_reg;

    if (time != sun4i->hw.tm_reg->tmr_cur_value_reg) {
        time = sun4i->hw.tm_reg->tmr_cur_value_reg;
    }

    return ((uint64_t)((time) * NS_IN_S) / freq_divd[sun4i->clk_div]);//24000000ull);
}

int sun4i_start_timestamp_timer(sun4i_timer_t * sun4i)
{
    assert(sun4i != NULL);
    assert(sun4i->user_cb_event == LTIMER_OVERFLOW_EVENT);

    // disable timer 
    sun4i->hw.tm_reg->tmr_ctrl_reg &= ~SUN4I_TMR_EN_BIT & ~SUN4I_TMR_CLK_SRC_MASK;

    //set timer to count up monotonically
    sun4i->hw.tm_reg->tmr_intv_value_reg = 0xFFFFFFFF;
    
    // set prescaler and clock source (to make sure it present)
    sun4i->hw.tm_reg->tmr_ctrl_reg |= SUN4I_TMR_CLK_SRC_OSC24M;

    // store prescaller used
    sun4i->clk_div = 0;

    // store value
    sun4i->hw.tm_reg->tmr_ctrl_reg |= SUN4I_TMR_LOAD;
    
    // wait for reload bit to become 0
    while (sun4i->hw.tm_reg->tmr_ctrl_reg & SUN4I_TMR_LOAD){}

    // enable timer with configs 
    sun4i->hw.tm_reg->tmr_ctrl_reg |= SUN4I_TMR_EN_BIT;

    return 0;
}

int sun4i_set_timeout(sun4i_timer_t * sun4i, uint64_t ns, bool periodic)
{
    if (sun4i == NULL) {
        return EINVAL;
    }

    // disable timer 
    sun4i->hw.tm_reg->tmr_ctrl_reg &= ~SUN4I_TMR_EN_BIT & ~SUN4I_TMR_CLK_SRC_MASK;
    //sun4i->hw.tm_reg->tmr_ctrl_reg = 0;

    // timer mode 
    uint32_t tclrFlags = periodic ? 0 : SUN4I_SINGLE_COUNT_MODE;

    // load timer count 
    uint64_t ticks = freq_ns_and_hz_to_cycles(ns, freq_divd[0]);

    // setting a divider ()
    uint32_t f_divider = 0;

    // check if ticks fits into register
    if (ticks <= 0xFFFFFFFF)
    {
        // load directly
        sun4i->hw.tm_reg->tmr_intv_value_reg = (uint32_t)(ticks & 0xFFFFFFFF);
    }
    else
    {
        // try to adjust prescaler (low persion)
        uint64_t f = freq_cycles_and_ns_to_hz(0xFFFFFFFF, ns);
        
        uint32_t i = 0;
        for (i = 0; freq_divd[i] != 0; i++)
        {
            if (freq_divd[i] <= f)
            {
                break;
            }
        }
        
        f_divider = i;
        if ((1 << i) > 128)
        {
            // error
            ZF_LOGE("allwinnerD1 timer, can not set prescaler \
                (more than 128), too large timeout ns in %s:%d", __FUNCTION__, __LINE__);

            return EINVAL;
        }
        
        ticks = freq_ns_and_hz_to_cycles(ns, freq_divd[i]);
    }

    // set prescaler and clock source (to make sure it present)
    sun4i->hw.tm_reg->tmr_ctrl_reg |= (f_divider << 4) | SUN4I_TMR_CLK_SRC_OSC24M;

    // set timeout
    sun4i->hw.tm_reg->tmr_intv_value_reg  = (uint32_t)(ticks & 0xffffffff);

    // store value
    sun4i->hw.tm_reg->tmr_ctrl_reg |= SUN4I_TMR_LOAD;

    // store prescaller used
    sun4i->clk_div = f_divider;

    // wait for reload bit to become 0
    while (sun4i->hw.tm_reg->tmr_ctrl_reg & SUN4I_TMR_LOAD){}

    // enable timer with configs 
    sun4i->hw.tm_reg->tmr_ctrl_reg |= SUN4I_TMR_EN_BIT | tclrFlags;

    return 0;
}

static void sun4i_handle_irq(void *data, ps_irq_acknowledge_fn_t acknowledge_fn, void *ack_data)
{
    assert(data != NULL);
    sun4i_timer_t * sun4i = data;

    // in d1 user manual it is said to set bit to 1 to reset
    sun4i->hw.tm_irq->irq_sta_reg |= sun4i->irq_reset_mask;
    
    // ack any pending irqs 
    ZF_LOGF_IF(acknowledge_fn(ack_data), "Failed to acknowledge the timer's interrupts");
    if (sun4i->user_cb_token) {
        sun4i->user_cb_fn(sun4i->user_cb_token, sun4i->user_cb_event);
    }

    return;
}

bool sun4i_pending_match(sun4i_timer_t * sun4i)
{
    switch (sun4i->user_cb_event)
    {
        case LTIMER_TIMEOUT_EVENT:
            return 
                (sun4i->hw.tm_irq->irq_sta_reg & SUN4I_TMR_IRQ_EN_0) == SUN4I_TMR_IRQ_EN_0;

        case LTIMER_OVERFLOW_EVENT:
            return 
                (sun4i->hw.tm_irq->irq_sta_reg & SUN4I_TMR_IRQ_EN_1) == SUN4I_TMR_IRQ_EN_1;
        
        default:
            ZF_LOGE("allwinnerD1 can not identify timer user_cb_event %lu \
                in %s:%d", sun4i->user_cb_event, __FUNCTION__, __LINE__);
            return false;
    }
}

void sun4i_destroy(sun4i_timer_t * sun4i)
{
    int error;

    if (sun4i->irq_id != PS_INVALID_IRQ_ID) {
        error = ps_irq_unregister(&sun4i->ops.irq_ops, sun4i->irq_id);
        ZF_LOGF_IF(error, "Failed to unregister IRQ");
    }

    if (sun4i->hw.tm_reg != NULL) {
        sun4i_stop(sun4i);
    }

    if (sun4i->sun4i_map_base != NULL) {
        /* use base because sun4i_map is adjusted based on whether secondary */
        ps_pmem_unmap(&sun4i->ops, sun4i->pmem, (void *) sun4i->sun4i_map_base);
    }
}

static int irq_index_walker(ps_irq_t irq, unsigned curr_num, size_t num_irqs, void *token)
{
    sun4i_timer_t *sun4i = token;

    if (SUN4I_IRQ_CHOICE == curr_num) {
        irq_id_t registered_id = ps_irq_register(&sun4i->ops.irq_ops, irq, sun4i_handle_irq, sun4i);
        if (registered_id >= 0) {
            sun4i->irq_id = registered_id;
            sun4i->irq = irq;
        } else {
            /* Bail on error */
            return registered_id;
        }
    }

    return 0;
}

int sun4i_init(sun4i_timer_t * sun4i, ps_io_ops_t ops, sun4i_timer_config_t config)
{
    int error;

    if (sun4i == NULL) {
        ZF_LOGE("sun4i cannot be null as %s:%d", __FUNCTION__, __LINE__);
        return EINVAL;
    }

    sun4i->ops = ops;
    sun4i->user_cb_fn = config.user_cb_fn;
    sun4i->user_cb_token = config.user_cb_token;
    sun4i->user_cb_event = config.user_cb_event;
    sun4i->irq_reset_mask = SUN4I_TMR0_IRQ_PEND;

    ps_fdt_cookie_t *cookie = NULL;
    error = ps_fdt_read_path(&ops.io_fdt, &ops.malloc_ops, config.fdt_path, &cookie);
    if (error) {
        ZF_LOGE("allwinnerD1 timer failed to read path (%d, %s)", error, config.fdt_path);
        return error;
    }

    sun4i->sun4i_map_base = ps_fdt_index_map_register(&ops, cookie, SUN4I_REG_CHOICE, &sun4i->pmem);
    if (sun4i->sun4i_map_base == NULL) {
        ZF_LOGE("allwinnerD1 timer failed to map registers");
        return ENODEV;
    }

    error = ps_fdt_walk_irqs(&ops.io_fdt, cookie, irq_index_walker, sun4i);
    if (error) {
        ZF_LOGE("allwinnerD1 timer failed to register irqs (%d)", error);
        return error;
    }

    sun4i->irq_id = ps_fdt_cleanup_cookie(&ops.malloc_ops, cookie);
    if (sun4i->irq_id) {
        ZF_LOGE("allwinnerD1 timer to clean up cookie (%d)", error);
        return sun4i->irq_id;
    }

    // map hw (timer 0)
    sun4i->hw.tm_irq = (void *)((uintptr_t)sun4i->sun4i_map_base);
    sun4i->hw.tm_reg = (void *)((uintptr_t)sun4i->sun4i_map_base) + SUN4I_TMR0_OFFSET;
    sun4i->clk_div = 0;

    // unmask the interrupt
    sun4i->hw.tm_irq->irq_enbl_reg |= SUN4I_TMR_IRQ_EN_0;
    
    return 0;
}

/* initialise rk using the base address of rkp, so that we do not attempt to map
 * that base address again but instead re-use it. */
int sun4i_init_secondary(sun4i_timer_t * sun4i, sun4i_timer_t *sun4i_tmr0, ps_io_ops_t ops, sun4i_timer_config_t config)
{
    int error;

    if (sun4i == NULL || sun4i_tmr0 == NULL) {
        ZF_LOGE("sun4i or sun4i_tmr0 cannot be null");
        return EINVAL;
    }

    sun4i->ops = ops;
    sun4i->user_cb_fn = config.user_cb_fn;
    sun4i->user_cb_token = config.user_cb_token;
    sun4i->user_cb_event = config.user_cb_event;
    sun4i->irq_reset_mask = SUN4I_TMR1_IRQ_PEND;

    /* so that destroy does not try to unmap twice */
    sun4i->sun4i_map_base = NULL;

    // rockchip64 has another timer in the same page at 0x20 offset
    // allwinnerD1 also has another timer at offset 0x20
    sun4i->hw.tm_irq = (void *)((uintptr_t)sun4i_tmr0->sun4i_map_base);
    sun4i->hw.tm_reg = (void *)((uintptr_t)sun4i_tmr0->sun4i_map_base) + SUN4I_TMR1_OFFSET;
    sun4i->clk_div = 0;

    /* similarly, the IRQ for this secondary timer is offset by 1 */
    sun4i->irq_id = PS_INVALID_IRQ_ID;
    ps_irq_t irq2 = { .type = PS_INTERRUPT, .irq.number = sun4i_tmr0->irq.irq.number + 1 };
    irq_id_t irq2_id = ps_irq_register(&ops.irq_ops, irq2, sun4i_handle_irq, sun4i);
    if (irq2_id < 0) {
        ZF_LOGE("Failed to register secondary irq for allwinnderD1 timer");
        return irq2_id;
    }
    sun4i->irq_id = irq2_id;

    // unmask the interrupt
    sun4i->hw.tm_irq->irq_enbl_reg |= SUN4I_TMR_IRQ_EN_1;

    return 0;
}

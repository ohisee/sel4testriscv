
#include <string.h>
#include <stdlib.h>
#include <platsupport/serial.h>
#include "../../chardev.h"

// - REG MAP
#define RBR         0x00 // 31:8 / 7:0 R RBR
#define THR         0x00 // 31:8 / 7:0 W THR
// UART_IER 0x0004
// 31:8 /
// 7 R/W 0x0 Programmable THRE Interrupt Mode Enable
// 6:5 /
// 4 R/W 0x0 RS485 Interrupt Enable
// 3 R/W 0x0 Enable Modem Status Interupt
// 2 R/W 0x0 Enable Receiver Line Status Interrupt
// 1 R/W 0x0 Enable Transmit Holding Register Empty Interrupt
// 0 R/W 0x0 Enable Received Data Available Interrupt
#define IER         0x04
#define LSR         0x14

// - RBR
#define RBR_MASK    MASK(8)

// - IER
#define IER_ERBFI   BIT(0)

// - LSR
// 5 R 0x1 TX holding register empty
#define LSR_THRE BIT(5)
// 0 R 0x0  Data ready (0 no data ready, 1 data ready)
#define LSR_DR BIT(0)

#define REG_PTR(base, off)     ((volatile uint32_t *)((base) + (off)))

int uart_getchar(ps_chardevice_t *d)
{
    int ch = EOF;

    if (*REG_PTR(d->vaddr, LSR) & LSR_DR) {
        ch = *REG_PTR(d->vaddr, RBR) & RBR_MASK;
    }
    return ch;
}

int uart_putchar(ps_chardevice_t* d, int c)
{
    while (!(*REG_PTR(d->vaddr, LSR) & LSR_THRE)) {
        continue;
    }
    *REG_PTR(d->vaddr, THR) = c;
    if (c == '\n' && (d->flags & SERIAL_AUTO_CR)) {
        uart_putchar(d, '\r');
    }

    return c;
}

static void
uart_handle_irq(ps_chardevice_t* d UNUSED)
{
    /* nothing to do */
}

int uart_init(const struct dev_defn* defn,
              const ps_io_ops_t* ops,
              ps_chardevice_t* dev)
{
    memset(dev, 0, sizeof(*dev));

    void* vaddr = chardev_map(defn, ops);
    if (vaddr == NULL) {
        return -1;
    }

    /* Set up all the  device properties. */
    dev->id         = defn->id;
    dev->vaddr      = (void*)vaddr;
    dev->read       = &uart_read;
    dev->write      = &uart_write;
    dev->handle_irq = &uart_handle_irq;
    dev->irqs       = defn->irqs;
    dev->ioops      = *ops;
    dev->flags      = SERIAL_AUTO_CR;

    // enable received data availabe interrupt
    *REG_PTR(dev->vaddr, IER) = IER_ERBFI;

    return 0;
}

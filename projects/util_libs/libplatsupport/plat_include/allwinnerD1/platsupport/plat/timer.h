/*
 * Copyright 2019, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */
#pragma once

#include <platsupport/fdt.h>
#include <platsupport/ltimer.h>
#include <platsupport/timer.h>

#define SUN4I_TIMER_PATH "/soc@3000000/timer@2050000"

#define SUN4I_REG_CHOICE 0
#define SUN4I_IRQ_CHOICE 0

// IRQ status register
#define SUN4I_TMR0_IRQ_PEND BIT(0) // bit (0)
#define SUN4I_TMR1_IRQ_PEND BIT(1) // bit (1)

// TIMER CTRL REG
#define SUN4I_TMR_EN_BIT BIT(0)
#define SUN4I_TMR_LOAD BIT(1)
#define SUN4I_TMR_CLK_SRC_OSC24M BIT(2)
#define SUN4I_SINGLE_COUNT_MODE BIT(7) // USER_MODE
#define SUN4I_TMR_CLK_SRC_MASK BIT(3)|BIT(2)

// TMR_IRQ_EN_REG
#define SUN4I_TMR_IRQ_EN_0 BIT(0)
#define SUN4I_TMR_IRQ_EN_1 BIT(1)

// - OFFSETS
#define SUN4I_TMR0_OFFSET 0x10 // from base
#define SUN4I_TMR1_OFFSET 0x20 // form base


// - TMR_CTRL_REG
// 0 0x0 TMR_EN Tmer enable
#define TMR_EN BIT(0) //TCLR_STARTTIMER
// 3:2 0x1 (OSC24M) TMR_CLK_SRC Select clock source
// 7 R/W 0x0 TMR_MODE Timing mode


#define TISR_IRQ_CLEAR BIT(0)

static UNUSED timer_properties_t sun4i_properties = {
    .upcounter = true,
    .timeouts = true,
    .relative_timeouts = true,
    .periodic_timeouts = true,
    .bit_width = 32,
    .irqs = 1
};

/*
    uint32_t irq_enbl_reg; // 0x0000 timer IRQ enable register
    uint32_t irq_sta_reg; // 0x0004 timer status reg
    uint32_t pad0;  // 0x0008
    uint32_t pad1;  // 0x000C
    uint32_t tmr0_ctrl_reg; // 0x0010 timer0 control register
    uint32_t tmr0_intv_value_reg; // 0x0014 timer 0 interval value reg
    uint32_t tmr0_cur_value_reg; // 0x0018 timer 0 current value reg
    uint32_t pad2; // 0x001C
    uint32_t tmr1_ctrl_reg; // 0x0020 timer 1 control register
    uint32_t tmr1_intv_value_reg; // 0x0024 timer 1 interval value reg
    uint32_t tmr1_cur_value_reg; // 0x0028 timer 1 current value reg
*/

struct sun4i_timer_map_irq
{
    uint32_t irq_enbl_reg; // 0x0000 timer IRQ enable register
    uint32_t irq_sta_reg; // 0x0004 timer status reg
};

struct sun4i_timer_map_reg
{
    uint32_t tmr_ctrl_reg; // timer control register
    uint32_t tmr_intv_value_reg; // timer interval value reg
    uint32_t tmr_cur_value_reg; // timer current value reg
};

struct sun4i_timer_map
{
    struct sun4i_timer_map_irq * tm_irq;
    struct sun4i_timer_map_reg * tm_reg;
};

/*
struct rk_map {
    uint32_t load_count0;
    uint32_t load_count1;
    uint32_t current_value0;
    uint32_t current_value1;
    uint32_t load_count2;
    uint32_t load_count3;basebase
    uint32_t interrupt_status;
    uint32_t control_register;basebase
};
*/

typedef struct {
    /* set in init */
    ps_io_ops_t ops;
    ltimer_callback_fn_t user_cb_fn;
    void *user_cb_token;
    ltimer_event_t user_cb_event;  /* what are we being used for? */

    /* set in fdt helper */
    volatile struct sun4i_timer_map hw; // mapped addresses
    void * sun4i_map_base;  // base address
    pmem_region_t pmem;
    ps_irq_t irq;
    irq_id_t irq_id;
    uint32_t irq_reset_mask;
    uint32_t clk_div;   // normally base0, but if too large value is used, then differ
} sun4i_timer_t;

typedef struct {
    char *fdt_path;
    ltimer_callback_fn_t user_cb_fn;
    void *user_cb_token;
    ltimer_event_t user_cb_event;
} sun4i_timer_config_t;

int sun4i_init(sun4i_timer_t *sun4i, ps_io_ops_t ops, sun4i_timer_config_t config);
int sun4i_init_secondary(sun4i_timer_t *sun4i, sun4i_timer_t *rkp, ps_io_ops_t ops, sun4i_timer_config_t config);
int sun4i_start_timestamp_timer(sun4i_timer_t *sun4i);
int sun4i_stop(sun4i_timer_t *sun4i);
uint64_t sun4i_get_time(sun4i_timer_t *sun4i);
int sun4i_set_timeout(sun4i_timer_t *sun4i, uint64_t ns, bool periodic);
/* return true if a match is pending */
bool sun4i_pending_match(sun4i_timer_t *sun4i);
void sun4i_destroy(sun4i_timer_t *sun4i);

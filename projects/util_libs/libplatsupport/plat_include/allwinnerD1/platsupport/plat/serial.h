/*
 * Copyright 2019, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#define UART0_PADDR  0x2500000
#define UART1_PADDR  0x2500400
#define UART2_PADDR  0x2500800
#define UART3_PADDR  0x2500c00
#define UART4_PADDR  0x2501000
#define UART5_PADDR  0x2501400


#define UART0_IRQ    0x12
#define UART1_IRQ    0x13
#define UART2_IRQ    0x14
#define UART3_IRQ    0x15
#define UART4_IRQ    0x16
#define UART5_IRQ    0x17

enum chardev_id {
    RP_UART0,
    RP_UART1,
    RP_UART2,
    RP_UART3,
    RP_UART4,
    RP_UART5,
    /* Aliases */
    PS_SERIAL0 = RP_UART0,
    PS_SERIAL1 = RP_UART1,
    PS_SERIAL2 = RP_UART2,
    PS_SERIAL3 = RP_UART3,
    PS_SERIAL4 = RP_UART4,
    PS_SERIAL5 = RP_UART5,
    /* defaults */
    PS_SERIAL_DEFAULT = RP_UART0
};

#define DEFAULT_SERIAL_PADDR UART0_PADDR
#define DEFAULT_SERIAL_INTERRUPT UART0_IRQ

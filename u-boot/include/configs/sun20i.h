/* SPDX-License-Identifier: GPL-2.0+ */
/*
 * Placeholder wrapper to allow addressing Allwinner D1 (and later) sun20i
 * CPU based devices separately. Please do not add anything in here.
 */
#ifndef __CONFIG_H
#define __CONFIG_H

#include <configs/sunxi-common.h>

#define CONFIG_BOOTCOMMAND \
    "if mmc rescan; then " \
        "echo SD/MMC found on device ${mmc_dev};" \
        "fatload mmc 0 0x40a2c000 elfloader;" \
        "fatload mmc 0 0x40a32940 archive.archive.o.cpio;" \
        "bootelf 0x40a2c000;" \
    "fi;" \


#endif /* __CONFIG_H */

/*
 * Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
 *
 * SPDX-License-Identifier: BSD-2-Clause
 */

#pragma once

#include <autoconf.h>

/* First address in the virtual address space that is not accessible to user level */

/*#define seL4_UserTop 0x0000004000000000UL
#define physBase 0x40000000*/
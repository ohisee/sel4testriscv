#
# Copyright 2020, Data61, CSIRO (ABN 41 687 119 230)
#
# SPDX-License-Identifier: GPL-2.0-only
#

cmake_minimum_required(VERSION 3.7.2)

declare_platform(allwinnerD1 KernelPlatformAllwinnerD1 PLAT_ALLWINNERD1 KernelArchRiscV)

if(KernelPlatformAllwinnerD1)
    declare_seL4_arch(riscv64)
    config_set(KernelRiscVPlatform RISCV_PLAT "allwinnerD1")
    config_set(KernelPlatformFirstHartID FIRST_HART_ID 0)
    config_set(KernelOpenSBIPlatform OPENSBI_PLATFORM "generic")
    config_set(KernelOpenSBIPlatformDef PLATFORM_ALLWINNER_D1 "true")

    list(APPEND KernelDTSList "tools/dts/allwinnerD1.dts")
    list(APPEND KernelDTSList "src/plat/allwinnerD1/overlay-allwinnerD1.dts")
    declare_default_headers(
        TIMER_FREQUENCY 24000000
        PLIC_MAX_NUM_INT 223
        INTERRUPT_CONTROLLER drivers/irq/riscv_plic0.h
    )
else()
    unset(KernelPlatformFirstHartID CACHE)
endif()
